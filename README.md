# libchip
This project contains a simple library for interacting with C.H.I.P.'s gpio kernel interface, and a couple example programs.

`blink` simply blinks CSID0 once a second.

The `blinds` daemon creates a fifo file at `/var/run/blinds`, which it listens on for two strings: `raise` and `lower`. These directions manipulate CSID0 (motor on/off) and CSID1 (direction pin) in order to raise or lower a set of automatic blinds, as sorta given by [this website](http://www.me.umn.edu/courses/me2011/arduino/technotes/dcmotors/bidirectional/bidirMotor.html).
