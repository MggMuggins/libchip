pub mod pins;

use std::fmt::{Display, Formatter, self};
use std::fs::File;
use std::io::{Read, Write};
use std::path::PathBuf;

use failure::Error;

const GPIO_PSEUDO_FILE: &str = "/sys/class/gpio";

/// This macro generates the signal interrupt code
/// so that you don't have to. Effectively, it creates
/// a handler that calls `Pin::unexport` for every pin it
/// recieves, and then exits 0.
///
/// If your program infinitely loops through logic, use this to
/// prevent "resource busy" errors from Ctrl-C'ing your program and running
/// it again. Arguments to the macro are the pin id's from `pins`.
///
/// Note that pins that are set high while the code is running will NOT
/// be set low by this code before the process exits.
///
/// See `bins/` in the cargo project for example code.
#[macro_export]
macro_rules! sig_interrupt {
    ( $($pin:ident),* ) => {
        // Nasty hack to prevent the OS thinking a Pin is still in use
        // I sorta hate the way this is implemented
        extern fn sig_interrupt(_: i32) {
            $(
                libchip::Pin::unexport($pin);
            )*
            std::process::exit(0);
        }
        
        let hangup_action = nix::sys::signal::SigAction::new(
            nix::sys::signal::SigHandler::Handler(sig_interrupt),
            nix::sys::signal::SaFlags::empty(),
            nix::sys::signal::SigSet::empty());
        unsafe {
            nix::sys::signal::sigaction(nix::sys::signal::Signal::SIGINT, &hangup_action)
                .expect("Failed to subscribe interrupt handler");
        }
    }
}

/// The mode assigned to this pin.
///
/// Display is implemented for this enum as the integer
/// value of the appropriate variant.
pub enum PinMode {
    In,
    Out
}

impl Display for PinMode {
    /// Convert to a text representation
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let word = match self {
            PinMode::In => "in",
            PinMode::Out => "out"
        };
        write!(f, "{}", word)
    }
}

/// The value assigned to or read from a Pin.
///
/// Display is implemented for this enum as the integer
/// value of the appropriate variant.
pub enum PinVal {
    Low,
    High
}

impl Display for PinVal {
    /// Format this value as a unicode integer (0 or 1)
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let int_val = match self {
            PinVal::Low => 0,
            PinVal::High => 1
        };
        write!(f, "{}", int_val)
    }
}

impl From<[u8; 1]> for PinVal {
    /// Convert from an ascii character integer (0 or 1)
    fn from(val_list: [u8; 1]) -> Self {
        let val = val_list[0] - 48; // Ascii 0 = 48; Maybe try b'0' instead
        match val {
            0 => PinVal::Low,
            _ => PinVal::High
        }
    }
}

/// An interface over a GPIO Pin.
///
/// # Note
/// It is very helpful for this struct to be dropped (or `Pin::unexport`
/// be called on the appropriate pin from `libchip::pins`), because it will
/// prevent the OS thinking the Pin is still in use.
pub struct Pin {
    pin_id: u32
}

impl Pin {
    /// Reserve a gpio pin for use by this process. This also sets the mode,
    /// which helps to prevent undefined behavior.
    pub fn new(pin_id: u32, mode: PinMode) -> Result<Pin, Error> {
        let mut export_file = PathBuf::from(GPIO_PSEUDO_FILE);
        export_file.push("export");
        
        File::create(export_file)?
            .write(pin_id.to_string().as_bytes())?;
        
        let pin = Pin { pin_id };
        pin.set_mode(mode)?;
        
        Ok(pin)
    }
    
    /// Set the mode of the pin
    pub fn set_mode(&self, mode: PinMode) -> Result<(), Error> {
        let mut mode_file = PathBuf::from(GPIO_PSEUDO_FILE);
        mode_file.push(format!("gpio{}/direction", self.pin_id));
        
        File::create(mode_file)?
            .write(mode.to_string().as_bytes())?;
        
        Ok(())
    }
    
    /// Read the current value of the pin
    ///
    /// Will likely return an error if the pin is set to `PinMode::Input`
    pub fn read(&self) -> Result<PinVal, Error> {
        let mut value_file = PathBuf::from(GPIO_PSEUDO_FILE);
        value_file.push(format!("gpio{}/value", self.pin_id));
        
        let mut value: [u8; 1] = Default::default();
        File::open(value_file)?
            .read(&mut value)?;
        
        Ok(PinVal::from(value))
    }
    
    /// Set the value of the pin
    ///
    /// Will likely return an error if the Pin is set to `PinMode::Input`
    pub fn write(&self, value: PinVal) -> Result<(), Error> {
        let mut value_file = PathBuf::from(GPIO_PSEUDO_FILE);
        value_file.push(format!("gpio{}/value", self.pin_id));
        
        File::create(value_file)?
            .write(value.to_string().as_bytes())?;
        
        Ok(())
    }
    
    /// Tell the OS that a pin is not in use. DON'T use this function
    /// UNLESS `drop()` is not being called for the pin you're using for some reason.
    ///
    /// Do not attempt to use this function unless you know you have good reason.
    // Used for SIGINT, rather than implementing this in drop()
    pub fn unexport(pin_id: u32) {
        let mut unexport_file = PathBuf::from(GPIO_PSEUDO_FILE);
        unexport_file.push("unexport");
        
        if let Ok(mut file) = File::create(unexport_file) {
            let _err = file.write(pin_id.to_string().as_bytes());
        }
    }
}

impl Drop for Pin {
    fn drop(&mut self) {
        Pin::unexport(self.pin_id);
    }
}
