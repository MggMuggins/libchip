pub use self::xio::*;

pub const PWM0: u32 = 34;
pub const AP_EINT3: u32 = 35;
pub const TWI1_SCK: u32 = 47;
pub const TWI1_SDA: u32 = 48;
pub const TWI2_SCK: u32 = 49;
pub const TWI2_SDA: u32 = 50;
pub const LCD_D2: u32 = 98;
pub const LCD_D3: u32 = 99;
pub const LCD_D4: u32 = 100;
pub const LCD_D5: u32 = 101;
pub const LCD_D6: u32 = 102;
pub const LCD_D7: u32 = 103;
pub const LCD_D10: u32 = 106;
pub const LCD_D11: u32 = 107;
pub const LCD_D12: u32 = 108;
pub const LCD_D13: u32 = 109;
pub const LCD_D14: u32 = 110;
pub const LCD_D15: u32 = 111;
pub const LCD_D18: u32 = 114;
pub const LCD_D19: u32 = 115;
pub const LCD_D20: u32 = 116;
pub const LCD_D21: u32 = 117;
pub const LCD_D22: u32 = 118;
pub const LCD_D23: u32 = 119;
pub const LCD_CLK: u32 = 120;
pub const LCD_DE: u32 = 121;
pub const LCD_HSYNC: u32 = 122;
pub const LCD_VSYNC: u32 = 123;
pub const CSIPCK: u32 = 128;
pub const CSICK: u32 = 129;
pub const CSIHSYNC: u32 = 130;
pub const CSIVSYNC: u32 = 131;
pub const CSID0: u32 = 132;
pub const CSID1: u32 = 133;
pub const CSID2: u32 = 134;
pub const CSID3: u32 = 135;
pub const CSID4: u32 = 136;
pub const CSID5: u32 = 137;
pub const CSID6: u32 = 138;
pub const CSID7: u32 = 139;
pub const AP_EINT1: u32 = 193;
pub const UART1_TX: u32 = 195;
pub const UART1_RX: u32 = 196;

#[cfg(feature = "chip_4_3")]
mod xio {
    pub const XIO_P0: u32 = 408;
    pub const XIO_P1: u32 = 409;
    pub const XIO_P2: u32 = 410;
    pub const XIO_P3: u32 = 411;
    pub const XIO_P4: u32 = 412;
    pub const XIO_P5: u32 = 413;
    pub const XIO_P6: u32 = 414;
    pub const XIO_P7: u32 = 415;
}

#[cfg(feature = "chip_4_4")]
mod xio {
    pub const XIO_P0: u32 = 1016;
    pub const XIO_P1: u32 = 1017;
    pub const XIO_P2: u32 = 1018;
    pub const XIO_P3: u32 = 1019;
    pub const XIO_P4: u32 = 1020;
    pub const XIO_P5: u32 = 1021;
    pub const XIO_P6: u32 = 1022;
    pub const XIO_P7: u32 = 1023;
}
