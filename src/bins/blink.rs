use std::thread;
use std::time::Duration;

use libchip::{Pin, PinMode, PinVal, sig_interrupt};
use libchip::pins::CSID0;

/// Thins program blinks CSID0 at 1 second intervals
fn main() {
    sig_interrupt!(CSID0);
    
    let pin = Pin::new(CSID0, PinMode::Out)
        .expect("Unable to open pin CSID0");
    
    loop {
        pin.write(PinVal::High).unwrap();
        thread::sleep(Duration::from_millis(1000));
        pin.write(PinVal::Low).unwrap();
        thread::sleep(Duration::from_millis(1000));
    }
}
