use std::fmt::{Display, Formatter, self};
use std::fs::File;
use std::io::Read;
use std::str::FromStr;
use std::thread;
use std::time::Duration;

use failure::Error;
use nix::sys::stat;
use nix::unistd;

use libchip::{Pin, PinMode, PinVal, sig_interrupt};
use libchip::pins::{CSID0, CSID1, CSID2, CSID3};

const FIFO: &str = "/var/run/blinds";

enum Directive {
    Lower,
    Raise
}

impl FromStr for Directive {
    type Err = ();
    
    /// Returns Ok(Directive) if the string is "raise" or "lower",
    /// and Err(()) otherwise.
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "lower" => Ok(Directive::Lower),
            "raise" => Ok(Directive::Raise),
            _ => Err(())
        }
    }
}

impl Display for Directive {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Directive::Lower => write!(f, "LOWER"),
            Directive::Raise => write!(f, "RAISE")
        }
    }
}

// This program creates a fifo at /var/run/blinds if it doesn't already
// exist, and then listens on that fifo for actions. Writing `raise` to
// the fifo will set CSID1 `LOW`, and `lower` will set CSID1 `HIGH`.
//
// Pin Mapping:
// CSID0 - Mode::Out: Power pin for motor
// CSID1 - Mode::Out: Direction pin for motor
// CSID2 - Mode::In : Top Limit Switch
// CSID3 - Mode::In : Bottom Limit Switch
//
// Setting CSID1 LOW should result in CSID3 being set HIGH, while
// setting CSID1 HIGH should result in CSID2 being set HIGH.
fn run() -> Result<(), Error> {
    let power_pin = Pin::new(CSID0, PinMode::Out)?;
    let direction_pin = Pin::new(CSID1, PinMode::Out)?;
    let top_switch = Pin::new(CSID2, PinMode::In)?;
    let bottom_switch = Pin::new(CSID3, PinMode::In)?;
    
    // Make sure we know what the state is starting the loop
    power_pin.write(PinVal::Low)?;
    direction_pin.write(PinVal::Low)?;
    
    let mut fifo = File::open(FIFO)?;
    let mut contents: [u8; 5] = Default::default();
    
    // This is only inactive control, we assume the motor is not running
    loop {
        // I'd like to poll the fifo during my polling loop also, but that'll
        //   require a little more fancy *nixcraft.
        fifo.read(&mut contents)?;
        
        if let Ok(directive) = String::from_utf8_lossy(&contents).parse::<Directive>() {
            // We've been given a directive, now we start polling both switches and
            // the fifo to make sure that our states match the current directive.
            // Once the current directive is met, we go back to waiting for more
            // instructions.
            println!("directive recieved: {}", directive);
            loop {
                match directive {
                    Directive::Lower => {
                        direction_pin.write(PinVal::Low)?;
                        
                        match bottom_switch.read()? {
                            PinVal::High => {
                                power_pin.write(PinVal::Low)?;
                                break;
                            },
                            PinVal::Low => {
                                power_pin.write(PinVal::High)?;
                            }
                        }
                    },
                    Directive::Raise => {
                        direction_pin.write(PinVal::High)?;
                        
                        match top_switch.read()? {
                            PinVal::High => {
                                power_pin.write(PinVal::Low)?;
                                break;
                            },
                            PinVal::Low => {
                                power_pin.write(PinVal::High)?;
                            }
                        }
                    }
                }
                
                thread::sleep(Duration::from_millis(500));
            }
        } else {
            println!("Invalid fifo input");
        }
    }
}

fn main() {
    sig_interrupt!(CSID0, CSID1, CSID2, CSID3);
    
    if let Err(_) = stat::lstat(FIFO) {
        // TODO: Work on permissions
        unistd::mkfifo(FIFO, stat::Mode::S_IRWXU)
            .expect(&format!("Failed to create fifo: {}", FIFO));
    }
    
    run().unwrap();
}
