use std::time::Duration;
use std::thread;

use libchip::{Pin, PinMode, PinVal, sig_interrupt};
use libchip::pins::CSID0;

fn main() {
    sig_interrupt!(CSID0);
    
    let input_pin = Pin::new(CSID0, PinMode::In)
        .expect("Unabled to open pin CSID0");
    
    loop {
        match input_pin.read() {
            Ok(pin_val) => {
                match pin_val {
                    PinVal::High => {
                        println!("Read HIGH!");
                    },
                    PinVal::Low => {
                        
                    }
                }
            },
            Err(e) => {
                println!("error: {}", e);
            }
        }
        
        thread::sleep(Duration::from_millis(500));
    }
}
